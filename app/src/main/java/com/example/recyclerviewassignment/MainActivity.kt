package com.example.recyclerviewassignment

import android.app.Dialog
import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.LayoutDirection
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewassignment.Adapters.ItemAdapter
import com.example.recyclerviewassignment.databinding.ActivityMainBinding
import com.example.recyclerviewassignment.models.Country


class MainActivity : AppCompatActivity() {
    lateinit var binnding : ActivityMainBinding
    private lateinit var progress:ProgressDialog

    val list : ArrayList<Country> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binnding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binnding.root)


        progress = ProgressDialog(this)
        progress.setCancelable(false)
        progress.setMessage("Loading Data")
        progress.setTitle("Please Wait")
        progress.show()

        list.add(Country("India","http://www.geognos.com/api/en/countries/flag/IN.png"))
        list.add(Country("USA","http://www.geognos.com/api/en/countries/flag/US.png"))
        list.add(Country("Belgium","http://www.geognos.com/api/en/countries/flag/BE.png"))
        list.add(Country("Bangladesh","http://www.geognos.com/api/en/countries/flag/BD.png"))
        list.add(Country("Australia","http://www.geognos.com/api/en/countries/flag/AU.png"))
        list.add(Country("Sri Lanka","http://www.geognos.com/api/en/countries/flag/LK.png"))
        list.add(Country("India","http://www.geognos.com/api/en/countries/flag/IN.png"))
        list.add(Country("USA","http://www.geognos.com/api/en/countries/flag/US.png"))
        list.add(Country("Belgium","http://www.geognos.com/api/en/countries/flag/BE.png"))
        list.add(Country("Bangladesh","http://www.geognos.com/api/en/countries/flag/BD.png"))
        list.add(Country("Australia","http://www.geognos.com/api/en/countries/flag/AU.png"))
        list.add(Country("Sri Lanka","http://www.geognos.com/api/en/countries/flag/LK.png"))

        binnding.recyclerView.layoutManager=LinearLayoutManager(this)
        binnding.recyclerView.adapter= ItemAdapter(list)


        Handler().postDelayed({
            progress.dismiss()
        },3000)




    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
         MenuInflater(this).inflate(R.menu.menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.addmenuitem -> adddialog()
        }
        return true
    }

    private fun adddialog() {
        var d=Dialog(this)
        d.setCancelable(true)
        d.setContentView(R.layout.addcountry_dialog)
        d.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT)

        var nameedit=d.findViewById<EditText>(R.id.cname)
        var addb=d.findViewById<Button>(R.id.addb)

        addb.setOnClickListener{
            if (nameedit.text.toString().equals(ignoreCase = true, other = "")){
                nameedit.setError("Invalid Name")
                return@setOnClickListener
            }

            progress.show()

            list.add(0,Country(nameedit.text.toString(),""))
            binnding.recyclerView.adapter=ItemAdapter(list)

            Handler().postDelayed({
                progress.dismiss()
                d.dismiss()
            },3000)
        }

        d.show()


    }

}