package com.example.recyclerviewassignment.Adapters

import android.app.Dialog
import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.recyclerviewassignment.R
import com.example.recyclerviewassignment.models.Country

class ItemAdapter(list : ArrayList<Country>) : RecyclerView.Adapter<ItemAdapter.MyViewHolder>() {

    private var list=list
    private lateinit var context : Context

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var imageView : ImageView =itemView.findViewById(R.id.imageView)
        var name : TextView = itemView.findViewById(R.id.name)
        var delete : ImageView = itemView.findViewById(R.id.deletebtn)
        var itemView2=itemView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        context=parent.context
        var view=LayoutInflater.from(parent.context).inflate(R.layout.list_item,parent,false)

        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var cp=holder.adapterPosition
        Glide.with(context).load(list.get(cp).link).into(holder.imageView)
        holder.name.setText(list.get(cp).name)
        holder.delete.setOnClickListener{
            list.removeAt(cp)
            notifyItemRemoved(cp)
            notifyItemRangeChanged(cp,list.size)
        }
        holder.itemView2.setOnClickListener{
            manageDialog(list[cp],cp)
        }
    }

    private fun manageDialog(country: Country,cp : Int) {
        var d= Dialog(context)
        d.setCancelable(true)
        d.setContentView(R.layout.addcountry_dialog)
        d.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)


        var nameedit=d.findViewById<EditText>(R.id.cname)
        var addb=d.findViewById<Button>(R.id.addb)
        var title=d.findViewById<TextView>(R.id.textView3)

        addb.setText("Save Changes")
        title.setText("Edit Country")

        nameedit.setText(country.name)
        addb.setOnClickListener{
            if (nameedit.text.toString().equals(ignoreCase = true, other = "")){
                nameedit.setError("Invalid Name")
                return@setOnClickListener
            }
            var newname=nameedit.text.toString()
            val link=country.link
            list.removeAt(cp)
            notifyItemRemoved(cp)
            notifyItemRangeChanged(cp,list.size)

            list.add(cp,Country(newname,link))
            notifyItemInserted(cp)
            notifyItemRangeChanged(cp,list.size)
            d.dismiss()

        }

        d.show()


    }

    override fun getItemCount(): Int {
       return list.size

    }



}